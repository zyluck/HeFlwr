import time

from heflwr.monitor.process_monitor import FileMonitor
from heflwr.monitor.process_monitor import PrometheusMonitor
from heflwr.monitor.process_monitor import RemoteMonitor


def main_logic():
    i = 0
    while True:
        print(f'main running {i}')
        time.sleep(1)
        i += 1
        if i >= 10:
            print('main logic over')
            break


def forever_loop():
    while True:
        pass


if __name__ == '__main__':
    monitor = FileMonitor(file='./p_log.txt', interval=3)
    monitor.set_metrics(cpu=True, memory=True, network=True, power=False)
    monitor.start()
    main_logic()
    monitor.stop()
    print(monitor.summary())

    # monitor = PrometheusMonitor(port=8003, interval=3)
    # monitor.set_metrics(cpu=True, memory=True, network=True, power=False)
    # monitor.start()
    # main_logic()
    # monitor.stop()

    # monitor = RemoteMonitor(host="127.0.0.1:5000", interval=3, identifier="client1")
    # monitor.set_metrics(cpu=True, memory=True, network=True, power=False)
    # monitor.start()
    # main_logic()
    # monitor.stop()
