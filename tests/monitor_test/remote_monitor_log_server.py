from flask import Flask, request
from urllib.parse import unquote_plus

app = Flask(__name__)


@app.route('/log', methods=['POST'])
def handle_log():
    if request.method == 'POST':
        log_record = request.get_data(as_text=True)
        log_record = "{identifier} | {levelname} {name} {asctime} | {filename}:{lineno} | {message}".format(
            **{k: unquote_plus(v) for k, v in [pair.split('=') for pair in log_record.split('&')]}
        )
        print(log_record)
        return 'Logged successfully!', 200
    else:
        return 'Unsupported method', 405


@app.route('/simple_log', methods=['POST'])
def handle_simple_log():
    if request.method == 'POST':
        log_record = request.get_data(as_text=True)
        log_record = "{identifier} | {message}".format(
            **{k: unquote_plus(v) for k, v in [pair.split('=') for pair in log_record.split('&')]}
        )
        print(log_record)
        return 'Logged successfully!', 200
    else:
        return 'Unsupported method', 405


if __name__ == '__main__':
    app.run(debug=True)
