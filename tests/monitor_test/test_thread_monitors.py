import time

from monitor.thread_monitor.file_monitor import FileMonitor
from monitor.thread_monitor.prometheus_monitor import PrometheusMonitor
from monitor.thread_monitor.remote_monitor import RemoteMonitor


def main_logic():
    i = 0
    while True:
        print(f'main running {i}')
        time.sleep(1)
        i += 1
        if i >= 40:
            print('main logic over')
            break


def forever_loop():
    while True:
        pass


if __name__ == '__main__':
    monitor = FileMonitor(file='./log.txt', interval=3)
    monitor.set_metrics(cpu=True, memory=True, network=True, power=False)
    monitor.start()
    main_logic()
    monitor.stop()

    # monitor = PrometheusMonitor(port=8003, interval=3)
    # monitor.set_metrics(cpu=True, memory=True, network=True, power=False)
    # monitor.start()
    # main_logic()
    # monitor.stop()

    # monitor = RemoteMonitor(host="127.0.0.1:5000", interval=3)
    # monitor.set_metrics(cpu=True, memory=True, network=True, power=False)
    # monitor.start()
    # main_logic()
    # monitor.stop()
