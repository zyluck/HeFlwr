![](https://gitcode.com/zyluck/HeFlwr/blob/main/pictures/name_logo.svg)

> #### A toolkit for system-heterogeneous federated learning research and deployment 

## Getting Started
- [Install HeFlwr](https://gitcode.com/zyluck/HeFlwr/blob/main/docs/en/installation.md)
## API Reference
- [heflwr.fed](https://gitcode.com/zyluck/HeFlwr/blob/main/docs/en/api/fed.md)
- [heflwr.log](https://gitcode.com/zyluck/HeFlwr/blob/main/docs/en/api/log.md)
- [heflwr.monitor](https://gitcode.com/zyluck/HeFlwr/blob/main/docs/en/api/monitor.md)
- [heflwr.nn](https://gitcode.com/zyluck/HeFlwr/blob/main/docs/en/api/nn.md)